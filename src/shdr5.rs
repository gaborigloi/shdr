//! What we really need here is probably an expression tree using generalized algebraic data types (GADTs):
//! https://gist.github.com/Sgeo/b707a941e2460b25d59b

// test: http://cs.brown.edu/courses/csci1290/2011/results/final/psastras/
// more measurements: https://software.intel.com/en-us/blogs/2014/07/15/an-investigation-of-fast-real-time-gpu-based-image-blur-algorithms
// this optimization on PowerVR: http://xissburg.com/faster-gaussian-blur-in-glsl/
// HOG implementations: https://github.com/forresti/piggyHOG/tree/master/reference_code
// GPU HOG: http://link.springer.com/article/10.1007/s11554-014-0447-5
// Halide reductions: http://halide-lang.org/tutorials/tutorial_lesson_09_update_definitions.html

extern crate fnv;

/* use std::ops; */
use std::ops::{Add, Sub, Mul, Div};

struct NormalF64 {
    value: f64
}

impl NormalF64 {
    fn from_f64(value: f64) -> Option<Self> {
        if value.is_normal() {
            Some(NormalF64 { value: value })
        } else {
            None
        }
    }
}

// https://codereview.stackexchange.com/questions/67577/k-means-in-rust/75270#75270
//TODO convert to f32 and compute the hash of that?
impl Hash for NormalF64 {

}

enum Ivec2TextureOp {
    /// The coordinates of the current element.
    Position,
    Const(i32, i32),
    Add(Box<FloatTextureOp>, Box<FloatTextureOp>),
    Neg(Box<FloatTextureOp>, Box<FloatTextureOp>),
    Mul(Box<FloatTextureOp>, Box<FloatTextureOp>),
}

enum FloatTextureOp {
    Const(f64),
    /// An input texture with F32 format.
    Input(String),
    // We can use constant propagation to merge these linear operations.
    // These are commutative and associative:
    Add(Box<FloatTextureOp>, Box<FloatTextureOp>),
    Neg(Box<FloatTextureOp>, Box<FloatTextureOp>),
    Mul(Box<FloatTextureOp>, Box<FloatTextureOp>),
    Sub(Box<FloatTextureOp>, Box<FloatTextureOp>),
    // Division is neither associative, nor commutative, so the order matters.
    Div(Box<FloatTextureOp>, Box<FloatTextureOp>),
    // All the below functions are considered non-linear functions of their inputs,
    // since they will be evaluted in the shader by built-in GLSL functions that require
    // all their inputs.
    /// Texture access.
    Tex(Box<FloatTextureOp>, Ivec2TextureOp),
}

/// Public wrapper to expose operations to the user.
// Could express being addressable
// (containing at least one texture access) with phantom type parameters.
// Could also keep other information in Phantom marker types, such as OpenGL support.
struct FloatTextureExpr {
    operations: FloatTextureOp,
}

macro_rules! implement_op {
    ( $op_trait: ident, $op_fn: ident, $wrapper_name: ident, $enum_name:ident ) => {
        impl $op_trait for $wrapper_name {
            type Output = Self;
            fn $op_fn(self, rhs: Self) -> Self::Output {
                $wrapper_name {
                    operations: $enum_name::Add(Box::new(self.operations), Box::new(rhs.operations)),
                }
            }
        }
    }
}

impl Add for FloatTextureExpr {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        FloatTextureExpr {
            operations: FloatTextureOp::Add(Box::new(self.operations), Box::new(rhs.operations)),
        }
    }
}

/*impl Sub for FloatTextureExpr {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        FloatTextureExpr {
            operations: FloatTextureOp::Neg(Box::new(self.operations), Box::new(rhs.operations)),
        }
    }
}*/

implement_op!(Sub, sub, FloatTextureExpr, FloatTextureOp);
implement_op!(Mul, mul, FloatTextureExpr, FloatTextureOp);
implement_op!(Div, div, FloatTextureExpr, FloatTextureOp);

fn test() {
    let _ = FloatTextureOp::Add(Box::new(FloatTextureOp::Const(3.0)), Box::new(FloatTextureOp::Const(2.0)));
}
