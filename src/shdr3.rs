//! What we really need here is probably an expression tree using generalized algebraic data types (GADTs):
//! https://gist.github.com/Sgeo/b707a941e2460b25d59b

/*extern crate fnv;

/* use std::ops; */
use std::ops::{Add, Sub};
use std::default::Default;
use std::convert::From;
use std::collections::{BTreeSet, BTreeMap};

/// Texture access - a texel in the texture.
#[derive(PartialEq, Eq, Hash, Clone)]
struct TextureAccess {
    input: u8,
    x: TextureOp,
    y: TextureOp,
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct LinearTextureCombination {
    coefficients: BTreeMap<TextureAccess, f32>,
    constant: f32,
}

#[derive(PartialEq, Eq, Hash, Clone)]
// Could express linearity / non-linearity with phantom type parameters.
enum TextureOp {
    // We can use constant propagation to merge these linear operations.
    LinearOp(LinearTextureCombination),
    // These are commutative and associative:
    Add(BTreeSet<TextureOp>),
    Mul(BTreeSet<TextureOp>),
    // Division is neither associative, nor commutative, so the order matters.
    Div(Box<TextureOp>),
    // All the below functions are considered non-liner functions of their inputs,
    // since they will be evaluted in the shader by built-in GLSL functions that require
    // all their inputs.
    /// Texture access.
    Tex(Box<TextureOp>),
}

impl From<f32> for TextureOp {
    fn from(constant: f32) -> Self {
        let linear_op = LinearTextureCombination {
            coefficients: BTreeMap::new(),
            constant: constant,
        };
        TextureOp::LinearOp(linear_op)
    }
}

struct TextureExpr {
    accesses: BTreeSet<TextureAccess>,
    operations: TextureOp,
}

impl Add for TextureExpr {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        TextureExpr {
            accesses: self.accesses.union(&rhs.accesses).cloned().collect(),
            operations: TextureOp::Add(Box::new(self.operations), Box::new(rhs.operations)),
        }
    }
}

fn test() {
    let a = TextureOp::Add(Box::new(TextureOp::from(3.0)), Box::new(From::from(2.0)));
}*/
