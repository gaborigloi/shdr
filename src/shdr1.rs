/*extern crate fnv;

/*use std::ops;*/
use std::ops::Add;
use std::default::Default;
use std::collections::HashMap;
use std::collections::HashSet;

use std::hash::BuildHasherDefault;
use self::fnv::FnvHasher;

/// The fnv hasher, which is more efficient for smaller inputs.
/// It is not to denial-of-service attacks, which is not required here.
type Hasher = BuildHasherDefault<FnvHasher>;

/// Either x or y step offset for texture lookup,
/// depending on where it is used in FragOp::Tex.
//
// Linear function (so that it can be done by simply stretching the texture in the
// vertex shader of the quad) of the step size and the current location:
// tc = a*x + b*s + c
// where x is the current location, s is the step size,
// a = current_coeff, b = step_coeff, c = const_offset
#[derive(PartialEq, Eq, Hash, Clone)]
struct TexCoord {
    /// 'step' represents the step size in the source texture, NOT in the target texture.
    /// This is the size of one texel.
    /// For the u, v arguments of FragOp::Tex, the correct vertical and horizontal
    /// step size is used, respectively.
    step_coeff: i8
}

impl Default for TexCoord {
    /// Returns the coordinate corresponding to the location of the current texel.
    fn default() -> Self {
        TexCoord {
            step_coeff: 0
        }
    }
}

/*enum OffsetOp {
    Add(Box<FragOp>, Box<FragOp>),
    Mul(Box<FragOp>, Box<FragOp>),
    /// Represents the step size in the source texture, NOT in the target texture.
    /// This is the size of one texel.
    /// For the u, v arguments of FragOp::Tex, the correct vertical and horizontal
    /// step size is used, respectively.
    Step,
    Const(f32),
    Current
}*/

#[derive(PartialEq, Eq, Hash, Clone)]
struct TextureAccess {
    input: u8,
    x: TexCoord,
    y: TexCoord
}

enum FragOp {
    Add(Box<FragOp>, Box<FragOp>),
    Mul(Box<FragOp>, Box<FragOp>),
    Const(f32),
    Tex(TextureAccess)
}

struct FragExpr {
    accesses: HashSet<TextureAccess, Hasher>,
    operations: FragOp
}

impl Add for FragExpr {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        FragExpr {
           accesses: self.accesses.union(&rhs.accesses).cloned().collect(),
           operations: FragOp::Add(Box::new(self.operations), Box::new(rhs.operations))
        }
    }
}

/*impl FragOp {
    fn visit_nodes_mut<F>(&self, visitor: &mut F)
        where F: FnMut(&FragOp) {
        visitor(self);
        match *self {
            FragOp::Add(ref op1, ref op2) | FragOp::Mul(ref op1, ref op2) => {
                op1.visit_nodes_mut(visitor);
                op2.visit_nodes_mut(visitor);
            },
            _ => ()
        }
    }
}*/

// Could build up this hash table as we build up a struct containing FragOp and this hash table,
// instead of constructing it at compile time.
/*fn collect_tex_offsets(ops: FragOp) -> HashSet<TextureAccess, Hasher> {
    let fnv = BuildHasherDefault::<FnvHasher>::default();
    let mut tex_offsets = HashSet::with_hasher(fnv);
    ops.visit_nodes_mut(&mut |ops| {
        if let FragOp::Tex(ref texture_access) = *ops {
            tex_offsets.insert(texture_access);
        }
    });
    tex_offsets
}

fn compile(ops: FragOp) -> (String, String) {
    ("asdf".to_owned(), "jkfd".to_owned())
}

fn test() {
    let a = FragOp::Add(Box::new(FragOp::Const(3.0)), Box::new(FragOp::Const(2.0)));
}*/*/
