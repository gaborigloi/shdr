//! What we really need here is probably an expression tree using generalized algebraic data types (GADTs):
//! https://gist.github.com/Sgeo/b707a941e2460b25d59b

/*extern crate fnv;

/* use std::ops; */
use std::ops::{Add, Sub};
use std::default::Default;
use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::From;

use std::hash::BuildHasherDefault;
use self::fnv::FnvHasher;

type Hasher = BuildHasherDefault<FnvHasher>;

/// Texture access - a texel in the texture.
#[derive(PartialEq, Eq, Hash, Clone)]
struct TextureAccess {
    input: u8,
    x: FloatTextureOp,
    y: FloatTextureOp,
}

struct LinearTextureCombination {
    coefficients: HashMap<TextureAccess, f32>,
    constant: f32,
}

enum Vec2TextureOp {

}

enum FloatTextureOp {
    // We can use constant propagation to merge these linear operations.
    LinearOp(LinearTextureCombination),
    // These are commutative and associative:
    Add(HashSet<FloatTextureOp>),
    Mul(HashSet<FloatTextureOp>),
    // Division is neither associative, nor commutative, so the order matters.
    Div(Box<FloatTextureOp>),
    // All the below functions are considered non-liner functions of their inputs,
    // since they will be evaluted in the shader by built-in GLSL functions that require
    // all their inputs.
    /// Texture access.
    Tex(Box<FloatTextureOp>),
}

impl From<f32> for FloatTextureOp {
    fn from(constant: f32) -> Self {
        let fnv = BuildHasherDefault::<FnvHasher>::default();
        let linear_op = LinearTextureCombination {
            coefficients: HashMap::with_hasher(fnv),
            constant: constant,
        };
        FloatTextureOp::LinearOp(linear_op)
    }
}

// Could use phantom type parameters to provide additional information.
struct FloatTextureExpr {
    accesses: HashSet<TextureAccess, Hasher>,
    operations: FloatTextureOp,
}

impl Add for FloatTextureExpr {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        FloatTextureExpr {
            accesses: self.accesses.union(&rhs.accesses).cloned().collect(),
            operations: FloatTextureOp::Add(Box::new(self.operations), Box::new(rhs.operations)),
        }
    }
}

fn test() {
    let a = FloatTextureOp::Add(Box::new(FloatTextureOp::from(3.0)), Box::new(From::from(2.0)));
}*/
