//! What we really need here is probably an expression tree using generalized algebraic data types (GADTs):
//! https://gist.github.com/Sgeo/b707a941e2460b25d59b

/*extern crate fnv;

/* use std::ops; */
use std::ops::{Add, Sub};
use std::default::Default;
use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::From;

use std::hash::BuildHasherDefault;
use self::fnv::FnvHasher;

type Hasher = BuildHasherDefault<FnvHasher>;

#[derive(PartialEq, Eq, Hash, Clone)]
struct TexCoord {
    step_coeff: i8,
}

impl Default for TexCoord {
    fn default() -> Self {
        TexCoord { step_coeff: 0 }
    }
}

/// Texture access - a texel in the texture.
#[derive(PartialEq, Eq, Hash, Clone)]
struct TextureAccess {
    input: u8,
    x: TexCoord,
    y: TexCoord,
}

struct LinearTextureCombination {
    coefficients: HashMap<TextureAccess, f32, Hasher>,
    constant: f32,
}

struct TextureAccessProperties {
    /// The expression is a linear function of the given texture.
    ///
    /// In addition, all the indexes into the texture are constant in the expression.
    only_linear: bool,
}

// Could express linearity / non-linearity with phantom type parameters.
enum TextureOp {
    // We can use constant propagation to merge these linear operations.
    LinearOp(LinearTextureCombination),
    Add(Box<TextureOp>, Box<TextureOp>),
    Mul(Box<TextureOp>, Box<TextureOp>),
    // All the below functions are considered non-liner functions of their inputs,
    // since they will be evaluted in the shader by built-in GLSL functions that require
    // all their inputs.
    /// Indirect texture access.
    Tex(Box<TextureOp>),
}

impl From<f32> for TextureOp {
    fn from(constant: f32) -> Self {
        let fnv = BuildHasherDefault::<FnvHasher>::default();
        let linear_op = LinearTextureCombination {
            coefficients: HashMap::with_hasher(fnv),
            constant: constant,
        };
        TextureOp::LinearOp(linear_op)
    }
}

struct TextureExpr {
    accesses: HashSet<TextureAccess, Hasher>,
    operations: TextureOp,
}

impl Add for TextureExpr {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        TextureExpr {
            accesses: self.accesses.union(&rhs.accesses).cloned().collect(),
            operations: TextureOp::Add(Box::new(self.operations), Box::new(rhs.operations)),
        }
    }
}

fn test() {
    let a = TextureOp::Add(Box::new(TextureOp::from(3.0)), Box::new(From::from(2.0)));
}*/
