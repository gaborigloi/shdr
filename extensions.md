
* Pad texture with zero by using the truncate function at texel fetch
* Use f64 precision when compiling shader
    * Show relative, absolute truncation errors
    * Warn about underflow, ...

* Two possible APIs:
  1. shift(compose(i1, i2), 4) / i.compose(i2).shift_x(4)
  2. i.at(i2.at(x,y) - (4, 0)
  * Can implement *1.* in an extra module using *2.*

##Compilation:

* "Scheduling" - when compiling a function, may split into multiple passes with different
  shaders
  * If taps are too far for the platform
  * If the number of taps is too high for the platform
  * If it is more efficient to do multiple passes because there are too many
    taps, or they are too far and they are grouped at different locations.
* Intermediate AST representing needed subset of GLSL?
  * Could then have a separate module for compiling this into shader source
    code
    * -> A similar library for general GLSL shaders.

##Modules

* Core crate: only framework
* Operations crate: common, reusable operations: finite difference filters, ...

##Layers

* + Rust DSL for fragment and vertex shaders as lower layer (support multiple
  shader versions like glium for OpenGL versions)?
* Core module to manipulate one texture and perform necessary conversions.
  Layer on top of core to manipulate multidimensional data, which compiles
  functions manipulating arrays using the fixed-dimension types provided by the
  core module.
  * Perform necessary conversions where F32 textures are not supported:
    <https://mchouza.wordpress.com/2011/02/21/gpgpu-with-webgl-solving-laplaces-equation/>
* Another wrapper layer on top of that provides "views" for manipulating arbitrary
  data types, such as strings for NLP:
  * Each operation on the data type maps to an operation on the lower-layer AST.
  * The underlying lower-layer AST is not exposed to provide type safety.
  * Could also have the GPU matrix library as a "view"

##Optimization:

* use OpenGL swizzling
* Optimize set of similar programs by moving some into subroutines:
  https://github.com/tomaka/glium/blob/78635fe52d602d7e391aa93b67ae5004d2a1e421/examples/subroutines.rs
* Mipmap averaging / summing optimization
  * Could provide a special function
* Disable interpolation optimization where accuracy matters, such as `exp`,
  or when the weights are not exactly representable with interpolation
  * Pass Configuration to compiler defining when to use this optimization

##Optimize existing shaders:

* A crate for parsing shaders and then optimizing them and compiling them back
  to source again.
  * Can use <https://github.com/TimNN/glslang-sys>, the glslang reference compiler
    is suitable for programmatic use and can return the AST of the input
    source.

##Evaluation

* Compare efficiency, accuracy of compiled, automatically unrolled loops with that of loops in the shader
  * efficiency vs CPU-based programs
* If HOG implemented: compare to other systems: <https://github.com/forresti/piggyHOG/tree/master/reference_code>

#Possible extensions:

* GPU matrix library

#Backends:
* Should use an OpenGL backend trait that executes the shader instructions and
  uploads, downloads textures:
  * glium, gfx-rs, gl-rs

##More backends:
Could have multiple backends, since in the first pass we just build up
exactly the same AST that the user defined:
* py-videocore, ...
* Future technologies
  * such as OpenCL, OpenGL 4+, Vulkan,
  * and **SPIR-V**
* (proprietary, vendor-specific backends, such as Cuda)
* *CPU backends:* simple CPU, BLAS/LAPACK, ...
