* Filter graph framework: http://http.developer.nvidia.com/GPUGems/gpugems_ch27.html

Existing "shader" languages that abstract over and compile to different GLSL
versions?  Existing Rust libraries?
* This seems to be similar for Haskell: https://github.com/jaredloomis/andromeda,
  but arbitrary GLSL shaders can be written in it.
* SPOC & SAREK: GPGPU programming library for OCaml - supports Cuda and OpenCL&WebCL: http://mathiasbourgoin.github.io/SPOC/ , https://github.com/mathiasbourgoin/SPOC
  * **But interoperability with OpenGL planned: http://mathiasbourgoin.github.io/SPOC/**
* Owl - has optimisations: https://discuss.ocaml.org/t/owls-computation-graph/2326
  * GPU support is work in progress: http://ocaml.xyz/chapter/index.html
    * Only OpenCL engine is being worked on: http://ocaml.xyz/chapter/cgraph_intro.html#what-to-do-with-gpu
* list in https://github.com/lychee-eng/miro/issues/5:
  * https://github.com/autumnai/collenchyma, https://github.com/lychee-eng/parenchyma: CUDA & OpenCL & GPU programming
  * Arrayfire: has rust bindings, supports CUDA & OpenCL & CPU fallback: http://arrayfire.org/docs/index.htm
* http://lambdacube3d.com/related
* Run Rust code on NVIDIA GPU: https://github.com/japaric/nvptx

* https://github.com/aras-p/glsl-optimizer
* https://www.opengl.org/wiki/GLSL_:_recommendations
* https://www.opengl.org/wiki/GLSL_Optimizations

* https://github.com/stackgl/glslify
* https://github.com/mikolalysenko/glsl-read-float

Rust mathematics libraries for computer graphics:
* https://github.com/dche/glm-rs
  * https://github.com/dche/glm-color/
* https://github.com/bjz/cgmath

Possible to write a custom shader language:
* https://maikklein.github.io/post/shading-language-part1/

* What optimizations are already done by the shader compiler?

Check other DSLs:

#Reason for project:
* My laptop doesn't seem to support OpenCL, so even the new version of
  OpenCV cannot use the GPU. (GPU module of old version:http://docs.opencv.org/2.4/modules/gpu/doc/introduction.html)
* My laptop does not support compute shaders either: https://www.opengl.org/wiki/Compute_Shader,
  it only supports OpenGL 3.3
* It has an AMD GPU, not an NVIDIA one, so it does not support CUDA & nvptx
* harlan (https://github.com/eholk/harlan) seems to work with OpenCL
* It seems that Halide currently does not have reductions (mapping multiple pixels
  of the source to one destination pixel) for OpenGL, which is required
  for computing histograms (and for averaging speed and colour)
  * TODO check that this is true
    * See http://halide-lang.org/tutorials/tutorial_lesson_09_update_definitions.html,
    * and https://github.com/halide/Halide#limitations
* It seems that SPOC (& SAREK?) currently does not support OpenGL, only Cuda and OpenCL
  * TODO check that this is true
    * **It seems that adding OpenGL interoperability is planned, but is not currently being developed: http://mathiasbourgoin.github.io/SPOC/**
* owl does not yet support GPU, only OpenCL is being worked on
  * TODO Why not extend it with a GPGPU backend?
    * See http://ocaml.xyz/chapter/cgraph_intro.html
    * and http://ocaml.xyz/chapter/cgraph_intro.html#what-to-do-with-gpu
* Why not extend those libraries?
  * Rust + glium protect against resource leaks
  * glium has great performance as checks can be disabled due to its safety -> TODO verify?
  * Provides more type safety compared to OpenGL shader strings: http://www.cs.cornell.edu/~asampson/blog/opengl.html
* Collenchyma & Parenchyma and coaster do not support OpenGL, only Cuda and OpenCL
  * TODO Why not extend those libraries?
* Why not write a custom shading language like RLSL?
  * Probably requires new technologies (VULKAN, SPIR-V)

Existing libraries solutions for Rust -> GLSL:
* https://github.com/kmcallister/glassful
* https://github.com/tomaka/rust-to-glsl

##Other possible methods
* Why not just
  * compile to GPU
  * use macro to generate Rust, like Lia
