Images as functions I: (NxN) -> R
* Can index intermediate expressions

* Override index[] syntax for convenient texture access: t[vec(1.0,2.0)]
  * Integer texture coordinates may not be necessary? -
    * Should consider what happens when interpolating linearly between the
      off-centre texture accesses
      * This results in double linear interpolation
    * But textures are represented as a grid of values: (NxN) -> R
  * Could use int for shader indexing, and float for bilinear interpolation:
    * `a[x,y]` -> int
    * `bilin_interp(a, u, v)` -> double

* (Constant) loops are automatically unrolled

* Can automatically infer output image size

#Tap optimization:

If there are multiple choices, just copy the current state and check both,
and choose the maximum.

Can't just blindly optimize everything, this might result in lower quality
or more texture accesses:

```
A B C
D E F
G H I
```

* `1/(A+B)+1/(B+E)+1/(E+D)+1/(D+A)` -> (A,B),(B,E),(E,D),(D,A) -> same number
  of accesses, worse accuracy
* (A,B),(B,C),(C,F),(E,F),(B,E),(D,E),(A,D) -> more accesses when optimized all pairs
  (Since the curve of length-2 edges is closed in these two cases,
  the number of optimized accesses >= number of taps.)
* (D,E),(E,F),(E,H),(B,E) -> optimization helps in this case
* And also in this case: DE,BE,EF,EH
* And AB,BE,AD

Optimization results in more accesses:
```
ABCDE
1--
 2--
  3--
```

Results in less accesses:
```
ABCD
1--
 2--
```

* Is this an NP-complete problem when there are multiple expressions?

#Compilation phases:

1. Original user-defined AST

2. Flatten into Add(Set<...>, const), Mul(Set<...>, const) with constant propagation
   * Also propagate Div, ... in case of a constant
   * Push constant Mul into Add - also helps with [MAD](https://www.opengl.org/wiki/GLSL_Optimizations#Get_MAD)
     OpenGL shader optimization

3. Flatten and extract LinearTextureExpr

4. Compute expression hashes

5. Common-(sub)expression elimination
   Create Var list.

6. Send taps to texture access optimizer, which returns the optimized
   texture accesses with their weights.

These can transform tree using postorder_map:
```
  A
 / \  -> f(A(f(B),f(C))
 B C
```

compile(f(tex("name), uniform("name")))

#Hashmaps used:

* For Vars obtained from CSE
* For semantic equivalence to discover more common expressions:
  BTreeMap<BTreeMap<Expr, i32>, f32> -> 3ab^2+2c+1 -> `[[a->1,b->2]->3,[c->1]->2,[]->1]`
  Keep track of expanded normal forms of expressions to check equality:
  a*(b+c) -> ab+ac
  * Update expanded form for Mul, Div, Add, Sub operations, wrap it for non-linear Exp, ... operations
* Texture accesses: texture name -> (expression hash, containing LinearTextureExpr hash) -> (weight, offset)

#Tests

* [Iterative optical flow](http://cs.brown.edu/courses/csci1290/2011/results/final/psastras/):
  system uses nearest-neighbour interpolation for
  the runtime texture access, this reduces the accuracy of the Gaussian
  computed
* Usage:
  * For a simple "Gaussian" g = t(x) + t(x+1) + t(x+2):
      * Downsampled gaussian of the original image: g(f(x))(2*x)
        * Generated texture accesses after shifting the texture accesses in the Gaussian function:
          t(2x) + t(2x+1) + t(2x+2)
      * Gaussian of the downsampled image: g(f(2*x))(x)
        * Produces t(2x) + t(2x + 2) + t(2x+4)
  * Can make general functions such as reduce, reduce_with_offset, colvolve/reduce_with_stride,
    that take a function, and return a different function
    * Can create convolutions with a convolve function, and change them
      to convolutions with stride using these:
      Convolution with stride is simply subsampling the convolution of the original image with the given stride.
